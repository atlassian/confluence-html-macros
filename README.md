# THIS REPOSITORY HAS BEEN FROZEN AND MIGRATED

This repository has been migrated to the confluence-public-plugins repository. This is part of the developer productivity team's efforts in bringing homogenous plugins closer together and allowing for overarching changes to be easier to manage. Please checkout the new repository in order to start working on this plugin.

* New home: [confluence-public-plugins](https://bitbucket.org/atlassian/confluence-public-plugins/src/master/) 
* Project status: [Confluence Monorepo Plugin Status](https://hello.atlassian.net/wiki/spaces/CSD/pages/2786862612/Confluence+Monorepo+plugin+migration+status) 
* Project overview: [Move closer to Confluence DC monorepo and colocation of tests](https://hello.atlassian.net/wiki/spaces/CSD/pages/2641350532/Move+closer+to+Confluence+DC+monorepo+and+colocation+of+tests) 
* Contact for help: Contact person on Project status page or [#conf-dc-thunderbird](https://atlassian.slack.com/archives/C01KJLNDB1D)

# Confluence HTML Macros
This repo contains a plugin which provides HTML macros which can be used on confluence pages.

## Prerequisites
* Java 8 or newer
* Maven 3.6.3 or newer
* A way to run an instance of confluence locally (e.g. Atlassian SDK)



## Installation
### 1. Start an instance of confluence on port 8080
### 2. Install the plugin on confluence
Run the following command from /plugin directory
```bash
mvn clean install confluence:install -DskipTests -Dhttp.port=8080
```

## Running Integration Tests

### 1. Run CTK Server
```bash
atlas-run-standalone --jvmargs "-Xmx2048m" --product ctk-server --version 0.23 --http-port 8990 --ajp-port 18990 --server localhost
```
### 2. Set VM Options
Add the following VM option to any integration test that you want to run.
```
-Dbaseurl=http://localhost:8080/confluence
```

### 3. You're good to go. Run those tests!
**Note**: You can also use ```mvn verify``` to boot up confluence, the ctk server, install the plugin then run all integration tests all in one go.


