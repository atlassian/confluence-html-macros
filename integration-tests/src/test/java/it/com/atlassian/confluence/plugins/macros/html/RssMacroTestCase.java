package it.com.atlassian.confluence.plugins.macros.html;

import com.atlassian.confluence.api.model.content.AttachmentUpload;
import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentRepresentation;
import com.atlassian.confluence.api.model.content.ContentType;
import com.atlassian.confluence.api.service.content.AttachmentService;
import com.atlassian.confluence.api.service.content.ContentService;
import com.atlassian.confluence.test.rest.api.ConfluenceRestClient;
import com.atlassian.confluence.test.rest.api.plugin.PluginRest;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.PageFixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import com.atlassian.confluence.test.stateless.rules.FlushIndexQueueRule;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.plugins.whitelist.testing.WhitelistTestRule;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import static com.atlassian.confluence.api.model.content.ContentBody.contentBodyBuilder;
import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.REGULAR_PERMISSIONS;
import static com.atlassian.confluence.test.stateless.fixtures.PageFixture.pageFixture;
import static com.atlassian.confluence.test.stateless.fixtures.SpaceFixture.spaceFixture;
import static com.atlassian.confluence.test.stateless.fixtures.UserFixture.userFixture;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.plugins.whitelist.testing.WhitelistTestRule.withDefaultAdminLoginAndBaseUrl;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.singleton;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(ConfluenceStatelessTestRunner.class)
public class RssMacroTestCase extends AbstractHtmlMacroTestCase {

    private static final String TEST_SPACE_KEY = "ds";

    @Fixture
    private static final UserFixture user = userFixture().build();
    @Fixture
    private static final SpaceFixture space = spaceFixture()
            .keyPrefix(TEST_SPACE_KEY)
            .permission(user, REGULAR_PERMISSIONS)
            .build();
    @Fixture
    private static final PageFixture page = pageFixture()
            .space(space)
            .author(user)
            .title("testEntityReferencesNotExpanded")
            .content("testEntityReferencesNotExpanded", ContentRepresentation.WIKI)
            .build();

    @Inject
    private static ConfluenceTestedProduct product;
    @Inject
    private static ConfluenceRestClient restClient;

    @Rule
    public FlushIndexQueueRule flushIndexQueueRule = new FlushIndexQueueRule();

    private WhitelistTestRule whitelistTestRule;

    @BeforeClass
    public static void initialise() {
        PluginRest pluginRest = restClient.getAdminSession().getPluginComponent();
        if (!pluginRest.isPluginEnabled(HTML_MACRO_PLUGIN)) {
            pluginRest.enablePlugin(HTML_MACRO_PLUGIN);
        }
        pluginRest.enablePluginModule(HTML_MACRO_PLUGIN, "rss");
    }

    @AfterClass
    public static void afterClass() {
        PluginRest pluginRest = restClient.getAdminSession().getPluginComponent();
        if (pluginRest.isPluginEnabled(HTML_MACRO_PLUGIN)) {
            pluginRest.disablePluginModule(HTML_MACRO_PLUGIN, "rss");
            pluginRest.disablePlugin(HTML_MACRO_PLUGIN);
        }
    }

    @Before
    public void setup() {
        whitelistTestRule = withDefaultAdminLoginAndBaseUrl(urlSelector.getBaseUrl());
        whitelistTestRule.disableWhitelist();
    }

    @After
    public void tearDown() {
        whitelistTestRule.removeCreatedWhitelistRules();
        whitelistTestRule.enableWhitelist();
    }

    /**
     * <a href="http://developer.atlassian.com/jira/browse/HTMLMACROS-11">HTMLMACROS-11</a>
     */
    //https://jira.atlassian.com/browse/CONFDEV-31857
    @Test
    public void testEntityReferencesNotExpanded() throws IOException {
        File tempFile = File.createTempFile("foo", StringUtils.EMPTY);
        File rssFile = File.createTempFile("rss", StringUtils.EMPTY);

        try {
            String fileContent = "HTMLMACROS-11";
            FileUtils.writeStringToFile(tempFile, fileContent, Charset.defaultCharset());

            String rssContentString = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>\n" +
                    "<!DOCTYPE foo [\n" +
                    "<!ENTITY " + tempFile.getName() + " SYSTEM \"" + tempFile.toURI() + "\">\n" +
                    "]>\n" +
                    "<rss version=\"2.0\">\n" +
                    "<channel>\n" +
                    "<title>&quot;Test RSS Exploit showing&quot; " + tempFile.toURI() + "</title>\n" +
                    "<description>this is " + tempFile.toURI() + ": &" + tempFile.getName() + ";</description>\n" +
                    "<item>\n" +
                    "<title>" + tempFile.getAbsolutePath() + "</title>\n" +
                    "<description>" + tempFile.toURI() + ": &" + tempFile.getName() + ";</description>\n" +
                    "<link>https://www.atlassian.com</link>\n" +
                    "</item>\n" +
                    "</channel>\n" +
                    "</rss>";
            FileUtils.writeStringToFile(rssFile, rssContentString, UTF_8);

            AttachmentService attachmentService = restClient.getAdminSession().attachmentService();
            final AttachmentUpload attachmentToCreate = new AttachmentUpload(rssFile, randomAlphanumeric(8),
                    "text/plain", "", false, false);
            final Content attachmentCreated = attachmentService.addAttachments(
                    page.get().getId(),
                    singleton(attachmentToCreate)
            ).getResults().get(0);

            ContentService contentService = restClient.createSession(user.get()).contentService();
            Content content = Content
                    .builder()
                    .type(ContentType.PAGE)
                    .id(page.get().getId())
                    .space(page.get().getSpace())
                    .title("testEntityReferencesNotExpanded")
                    .body(contentBodyBuilder()
                            .representation(ContentRepresentation.WIKI)
                            .value("{rss:url="
                                    + urlSelector.getBaseUrl()
                                    + "/download/attachments/"
                                    + page.get().getId().asLong()
                                    + "/"
                                    + attachmentCreated.getTitle()
                                    + "?os_username=" + user.get().getUsername()
                                    + "&os_password=" + user.get().getPassword()
                                    + "}"
                            ).build()
                    )
                    .version(page.get().getVersion().nextBuilder().build())
                    .build();
            contentService.update(content);

            product.loginAndView(user.get(), content);

            WebDriver driver = product.getTester().getDriver();
            assertFalse(driver.getPageSource().contains(fileContent));
            assertTrue(driver.getPageSource().contains("\"Test RSS Exploit showing\""));

            PageElement pageElement = pageElementFinder.find(By.xpath("//div[@class='rssMacro']"));
            waitUntilFalse(pageElement.timed().isPresent());
        } finally {
            tempFile.delete();
            rssFile.delete();
        }
    }
}
