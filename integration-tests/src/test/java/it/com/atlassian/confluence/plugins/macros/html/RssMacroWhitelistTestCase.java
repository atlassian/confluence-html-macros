package it.com.atlassian.confluence.plugins.macros.html;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentType;
import com.atlassian.confluence.rest.api.model.ExpansionsParser;
import com.atlassian.confluence.test.rest.api.ConfluenceRestClient;
import com.atlassian.confluence.test.rest.api.plugin.PluginRest;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.plugins.whitelist.testing.WhitelistTestRule;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.io.IOException;

import static com.atlassian.confluence.api.model.content.ContentRepresentation.WIKI;
import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.ANONYMOUS_PERMISSIONS;
import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.REGULAR_PERMISSIONS;
import static com.atlassian.confluence.test.stateless.fixtures.SpaceFixture.spaceFixture;
import static com.atlassian.confluence.test.stateless.fixtures.UserFixture.userFixture;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockserver.model.MediaType.APPLICATION_XML;

@RunWith(ConfluenceStatelessTestRunner.class)
public class RssMacroWhitelistTestCase extends AbstractHtmlMacroTestCase {

    private static final String MACRO_NAME = "rss";
    private static final String REMOTE_RESOURCE = "/download/attachments/950274/rssfeed.xml";

    @Fixture
    private static final UserFixture user = userFixture().build();
    @Fixture
    private static final SpaceFixture space = spaceFixture()
            .permission(user, REGULAR_PERMISSIONS)
            .anonymousPermission(ANONYMOUS_PERMISSIONS)
            .build();

    @Inject
    private static ConfluenceTestedProduct product;
    @Inject
    private static ConfluenceRestClient restClient;

    private static Content page;

    private WhitelistTestRule whitelistTestRule;

    @BeforeClass
    public static void initialise() {
        PluginRest pluginRest = restClient.getAdminSession().getPluginComponent();
        if (!pluginRest.isPluginEnabled(HTML_MACRO_PLUGIN)) {
            pluginRest.enablePlugin(HTML_MACRO_PLUGIN);
        }
        pluginRest.enablePluginModule(HTML_MACRO_PLUGIN, MACRO_NAME);

        // create page fixture here as they require the plugin modules to be enabled
        page = restClient.createSession(user.get()).contentService().create(
                Content.builder()
                        .space(space.get())
                        .title("HtmlIncludeFuncTest")
                        .type(ContentType.PAGE)
                        .body("{" + MACRO_NAME + ":url=" + remoteBaseUrl + REMOTE_RESOURCE + "}", WIKI)
                        .build(),
                ExpansionsParser.parse("body.wiki,space")
        );

        restClient.getAdminSession().permissions().enableAnonymousUseConfluence();
    }

    @AfterClass
    public static void afterClass() {
        PluginRest pluginRest = restClient.getAdminSession().getPluginComponent();
        if (pluginRest.isPluginEnabled(HTML_MACRO_PLUGIN)) {
            pluginRest.disablePluginModule(HTML_MACRO_PLUGIN, MACRO_NAME);
            pluginRest.disablePlugin(HTML_MACRO_PLUGIN);
        }

        restClient.getAdminSession().permissions().disableAnonymousUseConfluence();
    }

    @Before
    public void setup() {
        whitelistTestRule = WhitelistTestRule.withDefaultAdminLoginAndBaseUrl(urlSelector.getBaseUrl());
    }

    @Before
    public void configureMockServer() throws IOException {
        configureMockServer(REMOTE_RESOURCE, "/com/atlassian/confluence/plugins/macros/html/rssfeed.xml", APPLICATION_XML);
    }

    @After
    public void tearDown() {
        whitelistTestRule.removeCreatedWhitelistRules();
        product.logOutFast();
    }

    @Test
    public void testWhitelistDenyWithoutAnyRule() {
        assertRemoteContentBlocked();
    }

    @Test
    public void testWhitelistAllowUsingWildcard() {
        assertRemoteContentBlocked();
        whitelistTestRule.whitelistWildcard(remoteBaseUrl + "/*");
        assertRemoteContentIncluded();
    }

    @Test
    public void testWhitelistAllowUsingExactMatch() {
        assertRemoteContentBlocked();
        whitelistTestRule.whitelistExactUrl(remoteBaseUrl + REMOTE_RESOURCE);
        assertRemoteContentIncluded();
    }

    @Test
    public void testWhitelistAllowUsingRegex() {
        assertRemoteContentBlocked();
        whitelistTestRule.whitelistRegularExpression(remoteBaseUrl + "/download/attachments/[\\d]+/rssfeed.xml");
        assertRemoteContentIncluded();
    }

    @Test
    public void testWhiteListAnonymousDeny() {
        assertRemoteContentBlockedForAnonymous();
        whitelistTestRule.whitelistExactUrlDisallowAnonymous(remoteBaseUrl + REMOTE_RESOURCE);
        assertRemoteContentBlockedForAnonymous();
    }

    @Test
    public void testWhiteListAnonymousAllow() {
        assertRemoteContentBlockedForAnonymous();
        whitelistTestRule.whitelistExactUrl(remoteBaseUrl + REMOTE_RESOURCE);
        assertRemoteContentIncludedForAnonymous();
    }

    private void assertRemoteContentIncluded() {
        product.loginAndView(user.get(), page);
        assertRssFeedShown();
    }

    private void assertRemoteContentBlocked() {
        product.loginAndView(user.get(), page);
        assertRssFeedNotShown();
    }

    private void assertRemoteContentIncludedForAnonymous() {
        product.viewPage(page);
        assertRssFeedShown();
    }

    private void assertRemoteContentBlockedForAnonymous() {
        product.viewPage(page);
        assertRssFeedNotShown();
    }

    private void assertRssFeedNotShown() {
        PageElement pageElement = pageElementFinder.find(By.xpath("//div[@class='rssMacro']//tr[1]//a[text()='\"Test RSS Exploit showing\"']"));
        waitUntilFalse(pageElement.timed().isPresent());

        pageElement = pageElementFinder.find(By.xpath("//div[@class='wiki-content']//div[contains(@class,'errorBox')]//p//strong"));
        waitUntil(
                pageElement.timed().getText(),
                equalTo("Could not access the content at the URL because it is not from an allowed source.")
        );

        pageElement = pageElementFinder.find(By.xpath("//div[@class='wiki-content']//p[3]"));
        waitUntil(
                pageElement.timed().getText(),
                equalTo("You may contact your site administrator and request that this URL be added to the list of allowed sources.")
        );
    }

    private void assertRssFeedShown() {
        PageElement pageElement = pageElementFinder.find(By.xpath("//div[contains(@class,'rssMacro')]//tr[1]//a[text()='\"Test RSS Exploit showing\"']"));
        waitUntilTrue(pageElement.timed().isPresent());

        pageElement = pageElementFinder.find(By.xpath("//div[contains(@class,'rssMacro')]//tr[1]//span"));
        waitUntil(pageElement.timed().getText(), equalTo("This is description & test"));

        pageElement = pageElementFinder.find(By.xpath("//div[contains(@class,'rssMacro')]//tbody/tr//a"));
        waitUntil(pageElement.timed().getText(), equalTo("Items & Test"));

        pageElement = pageElementFinder.find(By.xpath("//div[contains(@class,'rssMacro')]//tbody/tr//span"));
        waitUntil(pageElement.timed().getText(), equalTo("Item description includes html markup"));
    }
}
