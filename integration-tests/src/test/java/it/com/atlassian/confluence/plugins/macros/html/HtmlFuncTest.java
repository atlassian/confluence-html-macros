package it.com.atlassian.confluence.plugins.macros.html;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentType;
import com.atlassian.confluence.api.service.content.ContentService;
import com.atlassian.confluence.rest.api.model.ExpansionsParser;
import com.atlassian.confluence.test.rest.api.ConfluenceRestClient;
import com.atlassian.confluence.test.rest.api.plugin.PluginRest;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static com.atlassian.confluence.api.model.content.ContentRepresentation.WIKI;
import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.REGULAR_PERMISSIONS;
import static com.atlassian.confluence.test.stateless.fixtures.SpaceFixture.spaceFixture;
import static com.atlassian.confluence.test.stateless.fixtures.UserFixture.userFixture;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.CoreMatchers.equalTo;

@RunWith(ConfluenceStatelessTestRunner.class)
public class HtmlFuncTest extends AbstractHtmlMacroTestCase {

    @Fixture
    private static final UserFixture user = userFixture().build();
    @Fixture
    private static final SpaceFixture space = spaceFixture()
            .permission(user, REGULAR_PERMISSIONS)
            .build();

    @Inject
    private static ConfluenceTestedProduct product;
    @Inject
    private static ConfluenceRestClient restClient;
    @Inject
    private static PageElementFinder pageElementFinder;

    private static PluginRest pluginRest;

    private static Content testHtmlWithHrefPage;
    private static Content testHtmlWithMacroPage;

    @BeforeClass
    public static void initialise() {
        pluginRest = restClient.getAdminSession().getPluginComponent();

        if (!pluginRest.isPluginEnabled(HTML_MACRO_PLUGIN)) {
            pluginRest.enablePlugin(HTML_MACRO_PLUGIN);
        }
        pluginRest.enablePluginModule(HTML_MACRO_PLUGIN, "html");
        pluginRest.enablePluginModule(HTML_MACRO_PLUGIN, "html-xhtml");

        // create page fixtures in here because they require the module to be enabled
        final ContentService contentService = restClient.createSession(user.get()).contentService();

        testHtmlWithHrefPage = contentService.create(
                Content.builder()
                        .space(space.get())
                        .title("HtmlFuncTest.testHtmlWithHref")
                        .type(ContentType.PAGE)
                        .body("{html}<a href=\"http://www.atlassian.com\">click here</a>{html}", WIKI)
                        .build(),
                ExpansionsParser.parse("body.wiki,space")
        );

        testHtmlWithMacroPage = contentService.create(
                Content.builder()
                        .space(space.get())
                        .title("HtmlFuncTest.testHtmlWithMacro")
                        .type(ContentType.PAGE)
                        .body("{html}<strong>{info}Info Text{info}</strong>{html}", WIKI)
                .build(),
                ExpansionsParser.parse("body.wiki,space")
        );
    }

    @AfterClass
    public static void teardown() {
        pluginRest = restClient.getAdminSession().getPluginComponent();
        if (pluginRest.isPluginEnabled(HTML_MACRO_PLUGIN)) {
            pluginRest.disablePluginModule(HTML_MACRO_PLUGIN, "html");
            pluginRest.disablePluginModule(HTML_MACRO_PLUGIN, "html-xhtml");
            pluginRest.disablePlugin(HTML_MACRO_PLUGIN);
        }
    }

    @Test
    public void testHtmlWithHref() {
        product.loginAndView(user.get(), testHtmlWithHrefPage);

        PageElement pageElement = pageElementFinder.find(By.xpath("//div[@class='wiki-content']//a[text()='click here']"));
        waitUntilTrue(pageElement.timed().isPresent());
    }

    /**
     * Test to make sure macros don't get rendered when its inside html macro
     */
    @Test
    public void testHtmlWithMacro() {
        product.loginAndView(user.get(), testHtmlWithMacroPage);
        PageElement pageElement = pageElementFinder.find(By.xpath("//div[@class='wiki-content']//strong[1]"));
        waitUntil(pageElement.timed().getText(), equalTo("{info}Info Text{info}"));
    }
}
