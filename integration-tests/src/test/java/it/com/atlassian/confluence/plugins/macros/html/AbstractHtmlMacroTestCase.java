package it.com.atlassian.confluence.plugins.macros.html;

import com.atlassian.confluence.test.ConfluenceBaseUrlSelector;
import com.atlassian.confluence.test.plugin.SimplePlugin;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.apache.commons.lang3.StringUtils;
import org.junit.Rule;
import org.mockserver.junit.MockServerRule;
import org.mockserver.model.MediaType;

import javax.inject.Inject;
import java.io.IOException;

import static com.google.common.io.Resources.getResource;
import static com.google.common.io.Resources.toByteArray;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

public abstract class AbstractHtmlMacroTestCase {

    protected static final String HTML_MACRO_PLUGIN_KEY = "confluence.macros.html";
    protected static final SimplePlugin HTML_MACRO_PLUGIN = new SimplePlugin(
            HTML_MACRO_PLUGIN_KEY, "Confluence HTML Macros"
    );
    protected static final String remoteBaseUrl = StringUtils.stripEnd(
            System.getProperty("baseurl.ctk-server", "http://localhost:8990"), "/");

    @Rule
    public MockServerRule mockServerRule = new MockServerRule(this, 8990);

    @Inject
    protected static ConfluenceBaseUrlSelector urlSelector;

    @Inject
    protected static PageElementFinder pageElementFinder;

    protected void configureMockServer(String requestPath, String resourceLocation, MediaType contentType) throws IOException {
        mockServerRule.getClient()
                .when(request().withMethod("GET")
                        .withPath(requestPath))
                .respond(response()
                        .withBody(toByteArray(getResource(getClass(), resourceLocation)))
                        .withContentType(contentType));
    }
}
