package com.atlassian.confluence.plugins.macros.html;

import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ReadOnlyApplicationLinkService;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.plugins.whitelist.OutboundWhitelist;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.sal.api.net.NonMarshallingRequestFactory;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.spring.container.ContainerContext;
import com.atlassian.spring.container.ContainerManager;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Objects.requireNonNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.isA;
import static org.mockito.Mockito.when;

public abstract class WhitelistedHttpRetrievalMacroTestCase<T extends WhitelistedHttpRetrievalMacro> {
    @Rule
    public final MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    protected I18NBean i18NBean;

    @Mock
    protected LocaleManager localeManager;

    @Mock
    protected I18NBeanFactory i18NBeanFactory;

    @Mock
    protected NonMarshallingRequestFactory<Request<?, Response>> requestFactory;

    @Mock
    protected ReadOnlyApplicationLinkService applicationLinkService;

    @Mock
    protected ApplicationLinkRequest applicationLinkRequest;

    @Mock
    protected ApplicationLinkRequestFactory applicationLinkRequestFactory;

    @Mock
    protected Response response;

    @Mock
    protected OutboundWhitelist whitelist;

    @Mock
    protected ContainerContext containerContext;

    @Mock
    protected UserManager userManager;

    @Mock
    protected VelocityHelperService velocityHelperService;

    protected UserKey mockUserKey = new UserKey("mockUserKey");

    Map<String, String> macroParameters;

    Page pageToBeRendered;
    T macro;

    @Before
    public void setUp() {
        macroParameters = new HashMap<>();
        pageToBeRendered = new Page();
        ContainerManager.getInstance().setContainerContext(containerContext);

        when(userManager.getRemoteUserKey()).thenReturn(mockUserKey);
        when(whitelist.isAllowed(isA(URI.class), eq(mockUserKey))).thenReturn(true);

        when(i18NBeanFactory.getI18NBean(any())).thenReturn(i18NBean);

        when(i18NBean.getText(anyString(), isA(List.class))).thenAnswer(returnsFirstArg());
        when(i18NBean.getText(anyString())).thenAnswer(returnsFirstArg());

        macro = createMacro();
    }

    String getClasspathResourceAsString(String resourcePath) throws IOException {
        try (Reader reader = new BufferedReader(new InputStreamReader(
                requireNonNull(getClass().getClassLoader().getResourceAsStream(resourcePath))));
             Writer writer = new StringWriter()) {

            IOUtils.copy(reader, writer);

            return writer.toString();
        }
    }

    protected abstract T createMacro();

    @Test
    public void testIgnoresEmptyUrl() throws MacroException {
        macroParameters.put("url", "");

        assertEquals("<span class=\"error\">whitelistedmacro.error.nourl</span>", macro.execute(macroParameters, null, pageToBeRendered.toPageContext()));
    }

    @Test
    public void testHasNoBody() {
        assertFalse(macro.hasBody());
    }

    @Test
    public void testDoesNotRenderBody() {
        assertEquals(RenderMode.NO_RENDER, macro.getBodyRenderMode());
    }

    @Test
    public void testIsNotInline() {
        assertFalse(macro.isInline());
    }
}
