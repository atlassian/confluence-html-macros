package com.atlassian.confluence.plugins.macros.html;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;

import java.util.Map;

/**
 * Reimplemented InlineHtmlMacro that doesn't try to be smart for the RTE the way the
 * {@link com.atlassian.renderer.v2.macro.basic.InlineHtmlMacro} does (at least in renderer 4 - 7).
 */
public class SimpleInlineHtmlMacro extends BaseMacro implements Macro
{
    @Override
    public TokenType getTokenType(Map parameters, String body, RenderContext context)
    {
        return TokenType.BLOCK;
    }

    @Override
    public boolean hasBody()
    {
        return true;
    }

    @Override
    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

    @Override
    public String execute(Map parameters, String body, RenderContext renderContext)
    {
        return body;
    }

    @Override
    public String execute(Map<String, String> params, String body, ConversionContext conversionContext)
    {
        return body;
    }

    @Override
    public BodyType getBodyType()
    {
        return BodyType.PLAIN_TEXT;
    }

    @Override
    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }
}
